# Subsonic

An incomplete pseudo-3d racer.

Currently supports curves but not hills. Decorations/pictures can be placed onto or beside the road, but don't render properly all the time (the incorrectness is not noticeable in gameplay). The car slows down when turning and going off the road; there is an animation for turning. Tracks are loaded from a file. No AI cars; obstructions can appear if added to the track but will not block the player car.

It took me a few tries to even get a basic pseudo-3d flat road to render properly, so originally this code was intended as just an example to get the basic road. As I experimented with turns and eventually the speed meter and roadside decorations, I ended up putting everything into the original .java file - not great design, but it worked at the time.

### Instructions
Download the [JAR](subsonic.jar).

`java -jar subsonic.jar` to run.

### About
Much of this project was based off of the content on the following two sites:

[Lou's Pseudo 3d Page](http://www.extentofthejam.com/pseudo/)

[Code inComplete JavaScript Racer](https://codeincomplete.com/posts/javascript-racer/)

I couldn't re-find the original source of the car sprites at this time. 

### Screenshots
![](screenshots/screenshot1.png)

A straight road, with decorations on it.

![](screenshots/screenshot2.png)

A curve in the road. The car graphic also turns, but for some reason this isn't visible in the screenshot.

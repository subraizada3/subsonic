/*
 * Copyright 2017 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.subraizada.subsonic.desktop;

import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.subraizada.subsonic.SSGame;

public class DesktopLauncher {

	private static final int WINDOW_SCALE = 4;
	private static final boolean FULLSCREEN = false;

	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		Graphics.DisplayMode desktopMode = LwjglApplicationConfiguration.getDesktopDisplayMode();

		if (FULLSCREEN) {
			config.setFromDisplayMode(desktopMode);
		} else {
			config.fullscreen = false;
			config.width = SSGame.SCREEN_WIDTH * WINDOW_SCALE;
			config.height = SSGame.SCREEN_HEIGHT * WINDOW_SCALE;
		}

		config.vSyncEnabled = true;
		new LwjglApplication(new SSGame(), config);
	}
}

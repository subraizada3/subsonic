/*
 * Copyright 2017 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.subraizada.subsonic;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.HashMap;

public class SSGame extends Game {

	// http://www.extentofthejam.com/pseudo/

	// TOOO:
	// decorations drawn back to front
	// background
	// make background turn when road turns
	// other cars
	// collision between cars
	// AI for bot cars

	// ENGINE LIMITATIONS:
	// no hills
	// turns don't feel realistic
	// can only have one decoration per line of road
	// car/road don't move at low speeds
	// incorrect scaling of road decorations

	// ASSET DESIGN;
	// to cover the whole road right to the edges, a decoration must be 630 px wide
	// at the bottom of this 630 px image, a 25 px thick border will cover the road borders

	private OrthographicCamera camera;
	private ShapeRenderer sr;
	private SpriteBatch sb;
	private Texture bgTex;
	private HashMap<Integer, Texture> cars;
	private HashMap<String, Texture> decorations;

	private String CURRENT_ROAD_NAME = "Test_Road";

	// display constants
	// 256x224
	public static int SCREEN_WIDTH = 398;
	public static int SCREEN_HEIGHT = 224;

	// camera constants
	// camera Y is height of camera above ground
	// camera Z is how far camera is behind the screen
	// camera offset is offset from z=0 in camera to z=0 in world
	private int vFOV = 100;
	private int hFOV = (int) (((float) SCREEN_WIDTH / SCREEN_HEIGHT) * vFOV); // used to draw background
	private int CAMERA_Y_MAX = 1300;
	private int CAMERA_Y_MAX_DECREASE = 400;
	private float CAMERA_MINIMUM_BLEND_SPEED = 0.5f; // speed, as % of max speed, at which to start lowering camera
	private int CAMERA_Y_MAX_CHANGE = 3;
	private int CAMERA_Y = CAMERA_Y_MAX;
	private int CAMERA_Z = (int) (SCREEN_WIDTH / 2 / Math.tan(Math.toRadians(vFOV / 2)));
	private int CAMERA_OFFSET = CAMERA_Z * CAMERA_Y / SCREEN_HEIGHT;

	// colors
	//private Color COLOR_SKY = new Color(34 / 255f, 141 / 255f, 203 / 255f, 1);
	private Color COLOR_SKY = new Color(0, 0, 1, 1);
	private Color COLOR_ROAD_LIGHT = new Color(142 / 255f, 140 / 255f, 142 / 255f, 1);
	private Color COLOR_ROAD_DARK = new Color(101 / 255f, 103 / 255f, 101 / 255f, 1);
	private Color COLOR_GRASS_LIGHT = new Color(0, 197 / 255f, 0, 1);
	private Color COLOR_GRASS_DARK = new Color(0, 154 / 255f, 0, 1);
	private Color COLOR_BORDER_LIGHT = new Color(1, 0, 0, 1);
	private Color COLOR_BORDER_DARK = new Color(1, 1, 1, 1);
	private Color COLOR_LANE = Color.WHITE;

	// road sizing
	private int ROAD_WIDTH_MAX = 300;
	private int ROAD_WIDTH_MIN = 20;
	private int ROAD_SEGMENTS_MAX = 120;
	private int ROAD_SEGMENTS_MAX_DECREASE = 10;
	private float ROAD_SEGMENTS_MINIMUM_BLEND_SPEED = 0.5f;
	private int ROAD_SEGMENTS_CURRENT = ROAD_SEGMENTS_MAX;
	private int ROAD_SEGMENT_SIZE = 120;
	private int ROAD_BORDER_WIDTH = 24;
	private int ROAD_LANE_LINE_WIDTH = 12;

	// road generation
	private int[] zVals = new int[ROAD_SEGMENTS_MAX]; // z map
	private int[] xVals = new int[zVals.length]; // x offsets for turning
	private float[] sVals = new float[zVals.length]; // scale factors
	private int[] roadWidths = new int[zVals.length]; // road widths

	// road map
	private String ROAD_FILES_PATH = "roads"; // directory containing road files
	// max turn strength is 0.045f
	private float[] roadGeom; // float is turn value for that line of road
	private String[][] roadDec; // left column is float, but stored as unparsed string
	// lines without decorations are left as [null, null]

	// moving the road
	private int colorPos = 0; // moves color segments
	private int roadPos = 0; // moves along the road and its generation (turns/hills)
	// car movement
	private int CAR_HEIGHT_MIN = 2;
	private int CAR_HEIGHT_BONUS_MAX = 25;
	private int CAR_HEIGHT_CURRENT = CAR_HEIGHT_MIN;
	private float ACCELERATION = 0.16f;
	private float BRAKING = 0.02f;
	private float DRAG = 0.02f; // e.g. drag = 0.02 means car keeps 98% of speed every tick
	private float GRASS_BONUS_DRAG = 0.03f; // added multiplicatively on top of DRAG when on grass
	private float TURNING_DRAG = 0.002f;
	// top speed occurs when (speed + acceleration) * (1 - drag) = speed
	// speed - drag * speed + accel - drag * accel = speed
	// accel - drag*accel = drag*speed
	// speed = (accel - drag*accel) / drag
	private float MAX_SPEED = (ACCELERATION - DRAG * ACCELERATION) / DRAG;
	private float speed = 0;
	private int TURN_SPEED = 6; // highest possible turn speed
	private int MIN_TURN_SPEED = 3; // minimum possible turn speed - used when car is fast
	private int turning = 0; // positive = right, negative = left, number = frame of turn image
	private float ROAD_OVERFLOW_LIMIT = 1.8f; // 1 means car can go up to edge of road
	private float carX = 0;
	private int CAR_WIDTH; // used for collision

	// speed meter
	private int SPEED_METER_INNER_SIZE = 50;
	private int SPEED_METER_OFFSET = (int) (SPEED_METER_INNER_SIZE * 0.25f);
	private int SPEED_METER_BORDER_SIZE = (int) (SPEED_METER_INNER_SIZE * 0.03f);
	private int SPEED_METER_NEEDLE_LONG_SIZE = (int) (SPEED_METER_INNER_SIZE * 0.9f);
	private int SPEED_METER_NEEDLE_SHORT_SIZE = (int) (SPEED_METER_INNER_SIZE * 0.2f);
	private int NEEDLE_FAST_ANGLE = 80; // speed is max
	private int NEEDLE_SLOW_ANGLE = 185; // speed = 0

	// background
	private int BG_DRAW_WIDTH; // pixel width of the bg texture to use to fill the width of the screen
	private int BG_HEIGHT; // height of the texture

	// only loop once
	private int oneLoopOnly = 0; // 0 = off; 1 = on

	@Override
	public void create() {
		camera = new OrthographicCamera();
		camera.setToOrtho(false, SCREEN_WIDTH, SCREEN_HEIGHT);
		sr = new ShapeRenderer();
		sr.setProjectionMatrix(camera.combined);
		sb = new SpriteBatch();
		sb.setProjectionMatrix(camera.combined);

		initBackground();
		cars = new HashMap<>(23, 2);
		initCarTextures();
		CAR_WIDTH = cars.get(0).getWidth();
		decorations = new HashMap<>(1);
		initDecorationTextures();

		genZVals();
		genSVals();
		genRoadWidths();
		genRoadGeometry(CURRENT_ROAD_NAME);
		genRoadDecorations(CURRENT_ROAD_NAME);
	}

	private void initBackground() {
		//bgTex = new Texture("background.png");
		//BG_DRAW_WIDTH = (int) ((hFOV / 360f) * bgTex.getWidth());
		//BG_HEIGHT = bgTex.getHeight();
	}

	private void initCarTextures() {
		for (int i = -11; i <= 11; i++) {
			cars.put(i, new Texture("cars/car" + i + ".png"));
		}
	}

	private void initDecorationTextures() {
		String[] decorationNames = {"bush", "startArch"}; // list of all decoration names
		for (String s : decorationNames) {
			decorations.put(s, new Texture("decorations/" + s + ".png"));
		}
	}

	private void genRoadGeometry(String roadName) {
		// generates the shape of road (curves)
		// file format:
		// file name = road_name.roadg, underscore = space
		// file lines: int fl.oat
		//   represents a turn with rate float, for duration int
		// file must end with newline
		// eg straight -> long right turn -> short left turn -> straight would be
		// 40 0 -> 120 0.4 -> 20 -0.4 -> 40 0
		BufferedReader br = null;
		LineNumberReader lr = null;
		try {
			// step 1: initialize file readers
			String roadFileName = ROAD_FILES_PATH + "/" + roadName + ".roadg";
			FileHandle file = Gdx.files.internal(roadFileName);
			br = file.reader(8192);
			lr = new LineNumberReader(file.reader());
			//br = new BufferedReader(new FileReader(roadFileName));
			//lr = new LineNumberReader(new FileReader(roadFileName)); // cant use same FileReader for both

			// step 2: count number of lines in file, make an int and float array to store file contents
			lr.skip(Long.MAX_VALUE);
			int numFileLines = lr.getLineNumber();
			int[] segmentLengths = new int[numFileLines];
			float[] segmentValues = new float[numFileLines];

			// populate road segment lengths & values
			for (int i = 0; i < numFileLines; i++) {
				String[] currentLine = br.readLine().split(" ");
				segmentLengths[i] = Integer.parseInt(currentLine[0]);
				segmentValues[i] = Float.parseFloat(currentLine[1]);
			}

			// find length of road
			int roadLength = 0;
			for (int i : segmentLengths) {
				roadLength += i;
			}

			// finally, make the road[]
			roadGeom = new float[roadLength];
			int currentPosInRoad = 0;
			for (int i = 0; i < numFileLines; i++) {
				for (int j = 0; j < segmentLengths[i]; j++) {
					roadGeom[currentPosInRoad] = segmentValues[i];
					currentPosInRoad++;
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		} finally {
			try {
				br.close();
				lr.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}

	private void genRoadDecorations(String roadName) {
		// generates road decorations (bushes/billboards along road, etc.)
		// file format:
		// same name as road geometry, but road_name.roadd instead of road_name.roadg
		// file lines: int string float
		//   represents the object with name string (e.g. "bush" for "bush.png") at road position int
		//   (this is distance along the track) at position float (this is horizontal, left edge of road
		//   = -1 and right edge of road = 1, center of screen = 0)
		// file must end with newline
		BufferedReader br = null;
		LineNumberReader lr = null;
		try {
			// step 1: initialize file readers
			String roadFileName = ROAD_FILES_PATH + "/" + roadName + ".roadd";
			FileHandle file = Gdx.files.internal(roadFileName);
			br = file.reader(8192);
			//br = new BufferedReader(new FileReader(roadFileName));

			// step 2: initialize road array
			roadDec = new String[roadGeom.length][2];
			// in a row, left column is position of object stored as unparsed float string, and right colum
			// is the object name; the row number is the position it occurs at along the road

			// step 3: load file into roadDec array
			// lines without decoration are left as null
			String line;
			while ((line = br.readLine()) != null) {
				String[] x = line.split(" ");
				int row = Integer.parseInt(x[0]); // convert object's position along track to int
				roadDec[row][0] = x[2]; // left column = object position
				roadDec[row][1] = x[1]; // right column = object name
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}

	@Override
	public void render() {
		switch (oneLoopOnly) {
			case -1:
				return;
			case 1:
				oneLoopOnly = -1;
				break;
		}
		///////////////////////////////////////////////////////////////////////
		///   game logic   ////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		int targetRoadSegments;
		if (speed > MAX_SPEED * ROAD_SEGMENTS_MINIMUM_BLEND_SPEED) {
			ROAD_SEGMENTS_CURRENT = ROAD_SEGMENTS_MAX - (int) (ROAD_SEGMENTS_MAX_DECREASE * (speed - MAX_SPEED * ROAD_SEGMENTS_MINIMUM_BLEND_SPEED) / (MAX_SPEED * ROAD_SEGMENTS_MINIMUM_BLEND_SPEED));
		} else {
			ROAD_SEGMENTS_CURRENT = ROAD_SEGMENTS_MAX;
		}

		if (speed > CAMERA_MINIMUM_BLEND_SPEED * MAX_SPEED) {
			int previousCameraY = CAMERA_Y;
			CAMERA_Y = CAMERA_Y_MAX - (int) (CAMERA_Y_MAX_DECREASE * (speed - MAX_SPEED * CAMERA_MINIMUM_BLEND_SPEED) / (MAX_SPEED * CAMERA_MINIMUM_BLEND_SPEED));
			// Y increased by more than max
			if (CAMERA_Y - previousCameraY > CAMERA_Y_MAX_CHANGE)
				CAMERA_Y = previousCameraY + CAMERA_Y_MAX_CHANGE;
			// Y decreased by more than max
			if (previousCameraY - CAMERA_Y > CAMERA_Y_MAX_CHANGE)
				CAMERA_Y = previousCameraY - CAMERA_Y_MAX_CHANGE;
		} else {
			int previousCameraY = CAMERA_Y;
			CAMERA_Y = CAMERA_Y_MAX;
			// Y increased by more than max
			if (CAMERA_Y - previousCameraY > CAMERA_Y_MAX_CHANGE)
				CAMERA_Y = previousCameraY + CAMERA_Y_MAX_CHANGE;
			// Y decreased by more than max
			if (previousCameraY - CAMERA_Y > CAMERA_Y_MAX_CHANGE)
				CAMERA_Y = previousCameraY - CAMERA_Y_MAX_CHANGE;
		}
		CAMERA_OFFSET = CAMERA_Z * CAMERA_Y / SCREEN_HEIGHT;
		genZVals();
		genSVals();
		genRoadWidths();
		genXVals(roadPos);
		updateXVals(); // compensate for road shifting when steering

		/*roadPos++;
		if (roadPos == roadGeom.length)
			roadPos = 0; // make the road loop from end -> beginning*/
		roadPos += speed;
		if (roadPos >= roadGeom.length)
			roadPos -= roadGeom.length;

		colorPos += speed;
		if (colorPos > ROAD_SEGMENT_SIZE)
			colorPos -= ROAD_SEGMENT_SIZE;


		///////////////////////////////////////////////////////////////////////
		///   rendering   /////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		sr.begin(ShapeRenderer.ShapeType.Filled);
		// clear screen and draw sky
		sr.setColor(COLOR_SKY);
		sr.rect(0, ROAD_SEGMENTS_CURRENT, SCREEN_WIDTH, SCREEN_HEIGHT - ROAD_SEGMENTS_CURRENT);

		// iterate through arrays
		for (int i = 0; i < ROAD_SEGMENTS_CURRENT; i++) {

			// set light/dark color segments
			boolean colorLight; // 0 dark; 1 light
			int zRemainder = (zVals[i] + (int) colorPos) % ROAD_SEGMENT_SIZE;
			colorLight = (zRemainder <= ROAD_SEGMENT_SIZE / 2);

			int roadWidth = roadWidths[i];
			int roadLeftPos = SCREEN_WIDTH / 2 - roadWidth + xVals[i];
			int roadRightPos = SCREEN_WIDTH / 2 + roadWidth + xVals[i];

			// draw road
			if (colorLight)
				sr.setColor(COLOR_ROAD_LIGHT);
			else sr.setColor(COLOR_ROAD_DARK);
			sr.rect(roadLeftPos + carX, i, roadWidth * 2, 1);

			// draw grass
			if (colorLight)
				sr.setColor(COLOR_GRASS_LIGHT);
			else sr.setColor(COLOR_GRASS_DARK);
			sr.rect(0, i, roadLeftPos + carX, 1);
			sr.rect(roadRightPos + carX, i, SCREEN_WIDTH - roadRightPos - carX, 1);

			// draw road borders
			if (colorLight)
				sr.setColor(COLOR_BORDER_LIGHT);
			else sr.setColor(COLOR_BORDER_DARK);
			int borderWidth = ROAD_BORDER_WIDTH * roadWidth / ROAD_WIDTH_MAX;
			sr.rect(roadLeftPos + carX, i, borderWidth, 1);
			sr.rect(roadRightPos - borderWidth + carX, i, borderWidth, 1);

			// draw lane lines, but only on the light segments
			if (colorLight) {
				sr.setColor(COLOR_LANE);

				int laneWidth = ROAD_LANE_LINE_WIDTH * roadWidth / ROAD_WIDTH_MAX;
				int roadPos1 = roadLeftPos + roadWidth * 2 / 3;
				int roadPos2 = roadRightPos - roadWidth * 2 / 3;
				sr.rect(roadPos1 - laneWidth / 2 + carX, i, laneWidth, 1);
				sr.rect(roadPos2 - laneWidth / 2 + carX, i, laneWidth, 1);
			}
		}
		sr.end();


		sb.begin();
		int currentRoadPos = roadPos;
		for (int i = 0; i < zVals.length; i++) {
			int roadWidth = roadWidths[i];

			// draw road decorations
			// these should be drawn in reverse order, back to front
			if (roadDec[currentRoadPos][1] != null) { // if there is a decoration on this line
				Texture tex = decorations.get(roadDec[currentRoadPos][1]);
				float texPos = Float.parseFloat(roadDec[currentRoadPos][0]);
				int texWidth = (int) (tex.getWidth() * sVals[i]);
				int texHeight = (int) (tex.getHeight() * sVals[i]);
				sb.draw(tex, SCREEN_WIDTH / 2 + xVals[i] + roadWidth * texPos - texWidth / 2 + carX, i, texWidth, texHeight);
			}

			// draw car
			CAR_HEIGHT_CURRENT = CAR_HEIGHT_MIN + (int) (CAR_HEIGHT_BONUS_MAX * speed / MAX_SPEED);
			//CAR_HEIGHT_CURRENT = 0;
			if (i == CAR_HEIGHT_CURRENT) {
				Texture currentCarTex = cars.get(turning);
				int carWidth = (int) (currentCarTex.getWidth() * sVals[i]);
				int carHeight = (int) (currentCarTex.getHeight() * sVals[i]);
				sb.draw(currentCarTex, SCREEN_WIDTH / 2 - carWidth / 2, i, carWidth, carHeight);
			}

			currentRoadPos++;
			if (currentRoadPos == roadGeom.length)
				currentRoadPos = 0;
		}

		sb.end();


		// draw speed meter, on top of road decorations
		sr.begin(ShapeRenderer.ShapeType.Filled);
		int speedMeterX = SCREEN_WIDTH - SPEED_METER_OFFSET;
		int speedMeterY = SPEED_METER_OFFSET;
		// background
		sr.setColor(Color.WHITE);
		sr.circle(speedMeterX, speedMeterY, SPEED_METER_INNER_SIZE + SPEED_METER_BORDER_SIZE);
		sr.setColor(Color.BLACK);
		sr.circle(speedMeterX, speedMeterY, SPEED_METER_INNER_SIZE);
		// calc and draw needle
		int needleAngleRange = NEEDLE_SLOW_ANGLE - NEEDLE_FAST_ANGLE;
		int needleAngle = (int) (NEEDLE_SLOW_ANGLE - ((speed / MAX_SPEED) * needleAngleRange));
		int needleTipX = (int) (SPEED_METER_NEEDLE_LONG_SIZE * MathUtils.cosDeg(needleAngle) + speedMeterX);
		int needleTipY = (int) (SPEED_METER_NEEDLE_LONG_SIZE * MathUtils.sinDeg(needleAngle) + speedMeterY);
		int needleBaseX = (int) (SPEED_METER_NEEDLE_SHORT_SIZE * MathUtils.cosDeg(needleAngle + 180) + speedMeterX);
		int needleBaseY = (int) (SPEED_METER_NEEDLE_SHORT_SIZE * MathUtils.sinDeg(needleAngle + 180) + speedMeterY);
		sr.setColor(Color.RED);
		sr.rectLine(needleTipX, needleTipY, needleBaseX, needleBaseY, 2);
		// needle cap
		sr.setColor(Color.BLACK);
		sr.circle(speedMeterX - 0.5f, speedMeterY - 0.5f, 1);
		sr.end();


		///////////////////////////////////////////////////////////////////////
		///   input   /////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		if (Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isKeyPressed(Input.Keys.W)) {
			speed += ACCELERATION;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isKeyPressed(Input.Keys.S)) {
			speed -= BRAKING;
			if (speed < 0)
				speed = 0;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isKeyPressed(Input.Keys.A)) {
			turning -= 1;
			if (turning < -5)
				turning = -5;
			carX += Math.max(MIN_TURN_SPEED, TURN_SPEED * (1 - (speed / MAX_SPEED)));
		} else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.D)) {
			turning += 1;
			if (turning > 5)
				turning = 5;
			carX -= Math.max(MIN_TURN_SPEED, TURN_SPEED * (1 - (speed / MAX_SPEED)));
		} else {
			if (turning != 0)
				turning -= Integer.signum(turning); // if turning is negative, add 1, and vice versa
		}
		int roadCenterPos = SCREEN_WIDTH / 2 + xVals[CAR_HEIGHT_CURRENT];
		// next two lines need to use same border width calculation used in draw code
		int roadLeftPos = (int) (roadCenterPos - roadWidths[0] * ROAD_OVERFLOW_LIMIT + (ROAD_BORDER_WIDTH * roadWidths[0] / ROAD_WIDTH_MAX));
		int roadRightPos = (int) (roadCenterPos + roadWidths[0] * ROAD_OVERFLOW_LIMIT - (ROAD_BORDER_WIDTH * roadWidths[0] / ROAD_WIDTH_MAX));
		float carPosAbsolute = SCREEN_WIDTH / 2 + xVals[CAR_HEIGHT_CURRENT] + carX;
		float carPosLeft = carPosAbsolute - CAR_WIDTH / 2;
		float carPosRight = carPosAbsolute + CAR_WIDTH / 2;
		if (carPosLeft < roadLeftPos) {
			float outOfBounds = roadLeftPos - carPosLeft;
			carX += outOfBounds;
			speed *= 1 - (outOfBounds / 200f);
		}
		if (carPosRight > roadRightPos) {
			float outOfBounds = roadRightPos - carPosRight;
			carX += outOfBounds;
		}
		// if the car is in a curve, move the car so as to not automatically follow the curver
		int carPosOnRoad = roadPos + CAR_HEIGHT_CURRENT;
		if (carPosOnRoad >= roadGeom.length) {
			carPosOnRoad -= roadGeom.length;
		}
		carX += 80 * speed * roadGeom[carPosOnRoad];
		if (turning != 0) {
			speed *= 1 - Math.abs(turning) * TURNING_DRAG;
		}
		speed *= 1 - DRAG;
		if (carX > roadWidths[0] || carX < -roadWidths[0]) { // car on grass
			speed *= 1 - GRASS_BONUS_DRAG;
		}
		// top speed occurs when (speed + acceleration) * (1 - drag) = speed

		// quick reset
		if (Gdx.input.isKeyPressed(Input.Keys.R)) {
			quickReset();
		}
	}

	private void quickReset() {
		roadPos = 0;
		speed = 0;
		carX = 0;
	}

	private void genZVals() {
		// convert screen y coords into world z coords
		// formula: (distance is z distance from camera to screen: CAMERA_Z)
		// offset = (distance * camera height) / screen height
		// world z = (distance * camera height / screen y) - offset
		// ^ screen y is inverse (SCREEN_HEIGHT - screen y)
		for (int i = 0; i < zVals.length; i++) {
			zVals[i] = (CAMERA_Z * CAMERA_Y / (SCREEN_HEIGHT - i)) - CAMERA_OFFSET;
		}
	}

	private void genRoadWidths() {
		// get road width based on scale factors
		// if scale = 0 -> return min road width
		// if scale = 1 -> return max road width
		for (int i = 0; i < roadWidths.length; i++) {
			roadWidths[i] = (int) (sVals[i] * (ROAD_WIDTH_MAX - ROAD_WIDTH_MIN) + ROAD_WIDTH_MIN);
		}
	}

	private void genSVals() {
		// get scale values
		for (int i = 0; i < sVals.length; i++) {
			sVals[i] = 1 - ((float) i / (sVals.length - 1));
		}
	}

	private void genXVals(int currentRoadPos) {
		float x = 0;
		float dx = 0;
		float ddx;
		for (int i = 0; i < xVals.length; i++) {
			if (currentRoadPos < roadGeom.length) {
				ddx = roadGeom[currentRoadPos];
			} else {
				int remainder = currentRoadPos - roadGeom.length;
				ddx = roadGeom[remainder];
			}
			currentRoadPos++;
			dx += ddx;
			x += dx;
			xVals[i] = (int) x;
		}
	}

	private void updateXVals() {
		float offsetPerLine = carX / ROAD_SEGMENTS_CURRENT;
		for (int i = 0; i < xVals.length; i++) {
			xVals[i] -= offsetPerLine * i;
		}
	}

	@Override
	public void dispose() {
		sr.dispose();
		sb.dispose();
		//bgTex.dispose();
		for (Texture tex : cars.values())
			tex.dispose();
		for (Texture tex : decorations.values())
			tex.dispose();
	}
}
